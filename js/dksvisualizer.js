(function($){
	$(document).ready(function(){
		if(globalContainerHeight <= 0) {
			try {
				globalContainerHeight = $('#wholecontainer').height();
			} catch(err) {
				globalContainerHeight = 0;
			}
		}
		zoomPage();
		DkSUI.init();
	});
})(jQuery);

$(window).on('resize', function(){ zoomPage(); });

globalContainerHeight = 0;

function zoomPage() {
	// http://jsfiddle.net/Matt_Coughlin/6XNxW/1/
	var maxH = 850;
	var maxH = Math.max(maxH, globalContainerHeight);
	var mobileH = 699;
	var multiplier = 0.97;
	var viewportHeight = $(window).height();
	if (viewportHeight > mobileH && maxH > (viewportHeight * multiplier)) {
		var pageHeight = $(document).height();
		var desiredSquareHeight = viewportHeight * multiplier;
		var desiredPageHeight = (desiredSquareHeight / maxH) * pageHeight;
		var zoom = (desiredPageHeight / pageHeight);
	} else {
		var zoom = 1;
	}
	$('body').css('zoom', zoom);
	$('body').css('-moz-transform', 'scale(' + zoom + ')');
	$('body').css(  '-o-transform', 'scale(' + zoom + ')');
}
