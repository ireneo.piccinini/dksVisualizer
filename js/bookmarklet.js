(function($){
	var origform = $('#ma')[0];
	if(!origform) return;
	var form = $('<form />').appendTo($('body'));
	$(form).attr('method', 'get');
	$(form).attr('target', '_blank');
	$(form).attr('action', 'http://www.startrekgalaxy.it/dksVisualizer/');
	window.formfields = {};
	var excludeIds = '#savedia, #armorfind, #custom-sort-pop, .settings, .fake';
	$('input, select', origform).each(function(index){
		if(!$(this).parents(excludeIds).length) {
			var sel = $(this)[0];
			var id = $(this).attr('name') || $(this).attr('id');
			if(!id || id == 'undefined') return;
			formfields[id] = $(this).attr('value');
			if(sel.tagName == 'SELECT') {
				var indVal = sel.options[sel.selectedIndex];
				if(indVal) formfields[id] = $(indVal).text();
			}
			if(sel.type == 'checkbox') formfields[id] = sel.checked ? 1 : 0;
		}
	});
	$('.weapons .tip', origform).not(':hidden').each(function(index){
		var sel = $(this)[0];
		var pre = $(this).attr('fo') || sel.className.replace(/^.*([lr]h\d).*$/i, '$1');
		var name, val;
		if(sel.tagName == 'IMG') {
			var name = sel.className.replace(/^.*(weapon|arrow).*$/i, '$1');
			var val = $(this).attr('typ');
		} else {
			var name = 'stat';
			var val = $(this).text();
		}
		var id = pre + '_' + name;
		formfields[id] = val;
	});
	if($('#btitle').length) {
		formfields['characterName'] = $('#btitle:first').text().replace(/^\s*Viewing build .(.+). by .+$/i, '$1');
	}
	$.each(formfields, function(nam, val){
		var input = $('<input type="hidden" />');
		$(input).attr({ name: nam, value: val });
		$(input).appendTo($(form));
	});
	$(form).submit();
})(jQuery);