var DkSUI = new Object({
	query: {},
	character: {},
	init: function() {
		$('#ui').load('ui.html', function(){
			$('#wholecontainer').removeClass('loading');
			// here the loading of the data
			DkSUI.parse();
			DkSUI.fillUI();
			// here's the displaying of the data
			$('#messages .loading.message').hide();
			$('.slot a').hover(function() {
				var $label_parents = $(this).parents('*[label]');
				var name, desc;
				if($label_parents.length) name = $label_parents.attr('label');
				var $desc_children = $('img[alt]', $(this));
				if($desc_children.length) {
					desc = $desc_children.attr('alt');
					var $mod_em = $('em[label]', $(this).parent());
					if($mod_em.length) desc = $mod_em.attr('label') + ' ' + desc;
				}
				if(name && desc) DkSUI.change_desc(name, desc);
			}, function() {
				DkSUI.change_desc(false, false);
			});
		});
	},
	parseQuery: function(queryString) {
		var query = queryString || window.location.search.substring(1);
		var vars = query.split('&');
		var queryObj = {};
		var i, pair, k, v;
		for (i=0; i<vars.length; i++) {
			pair = vars[i].split('=');
			k = decodeURIComponent(pair[0].replace(/\++/g, ' '))
			v = decodeURIComponent(pair[1].replace(/\++/g, ' '))
			queryObj[k] = v;
		}
		return queryObj;
	},
	parse: function(query) {
		var q = query || DkSUI.parseQuery();
		DkSUI.query = q;
		var k, i, cha = {};
		// initializing regexps
		re = {
			weapon_stats : [/(?:^\x28|(\D)\x28)([\w\s\.]+)\x29/g, '$1$2']
		};
		// parsing class and levels
		cha['class'] = q.startClass.replace(/^\s*\d+\s*\W\s*(\w)/, '$1') || 'Deprived';
		cha.characterName = q.characterName || 'No name';
		cha.covenant = q.covenant || 'No covenant';
		cha.souls = {
			level : q.csoulLevel || q.bsoulLevel || 1,
			spent : q.cst || '0',
			next  : q.csc || '?'
		};
		cha.points = {
			vitality     : q.cvitality || q.bvitality || 1,
			attunement   : q.cattunement || q.battunement || 1,
			endurance    : q.cendurance || q.bendurance || 1,
			strength     : q.cstrength || q.bstrength || 1,
			dexterity    : q.cdexterity || q.bdexterity || 1,
			resistance   : q.cresistance || q.bresistance || 1,
			intelligence : q.cintelligence || q.bintelligence || 1,
			faith        : q.cfaith || q.bfaith || 1
		};
		cha.humanity = {
			count : q.chumanity || '0'
		};
		// parsing stats
		cha.energy = {
			hp        : q.bhealth,
			stamina   : q.bstamina,
			equipload : q.bequip
		};
		cha.atk = {
			rweap1 : q.rh1_stat.replace(re.weapon_stats[0], re.weapon_stats[1]) || '0',
			rweap2 : q.rh2_stat.replace(re.weapon_stats[0], re.weapon_stats[1]) || '0',
			lweap1 : q.lh1_stat.replace(re.weapon_stats[0], re.weapon_stats[1]) || '0',
			lweap2 : q.lh2_stat.replace(re.weapon_stats[0], re.weapon_stats[1]) || '0'
		};
		cha.def = {
			physical  : q.bdef,
			strike    : q.bstrike,
			slash     : q.bslash,
			thrust    : q.bthrust,
			magic     : q.bmagic,
			flame     : q.bflame,
			lightning : q.blight
		};
		// parsing bonus
		cha.resist = {
			poise  : q.bpoise || '0',
			bleed  : q.bbleed || '0',
			poison : q.bposion || '0', // � davvero posion...
			curse  : q.bcurse || '0'
		};
		cha.discovery = {
			item: q.bitem || 100
		};
		cha.sorceries = {
			slots: q.battune || '0'
		};
		// parsing inventory
		cha.weapons = {
			rh : [
					{ name: q.rh1, em: q.rh1_weapon != 'normal' ? q.rh1_weapon : false },
					{ name: q.rh2, em: q.rh2_weapon != 'normal' ? q.rh2_weapon : false }
				],
			lh : [
					{ name: q.lh1, em: q.lh1_weapon != 'normal' ? q.lh1_weapon : false },
					{ name: q.lh2, em: q.lh2_weapon != 'normal' ? q.lh2_weapon : false }
				]
		};
		cha.items = [];
		for(i=1; i<=5; i++) if(q['item'+i] != 'No Item') cha.items.push(q['item'+i]);
		cha.quiver = {
			arrows : [],
			darts  : []
		};
		var ar, ark;
		for(k in cha.weapons) {
			for(i=1; i<=2; i++) {
				if((ar = q[k+i+'_arrow'])) {
					if(ar[0] == 'b') {
						ark = 'darts';
						ar = ar.substr(1);
						ar = ar.replace(/^light$/, 'lightning');
						ar += ' bolt';
					} else {
						ark = 'arrows';
						ar = ar.replace(/^firea$/, 'fire');
						ar = ar.replace(/^gdragon$/, 'dragonslayer');
						ar += ' arrow';
					}
					cha.quiver[ark].push(ar);
				}
			}
		}
		cha.armour = {
			head  : q.headgear,
			chest : q.chestgear,
			hands : q.handgear,
			legs  : q.leggear
		};
		cha.rings = [];
		for(i=1; i<=2; i++) if(q['ring'+i] != 'No Ring') cha.rings.push(q['ring'+i]);
		// parsing attunement
		cha.attunement = [];
		for(i=1; i<=12; i++) if(q['spell'+i] != 'No Spell') cha.attunement.push(q['spell'+i]);
		// all together now!
		DkSUI.character = cha;
		return cha;
	},
	fillUI: function(cha) {
		if(!cha) cha = DkSUI.character;
		var i, k;
		// name and covenant
		if(cha.characterName) $('#character_name').text(DkSUI.upperFirst(cha.characterName));
		if(cha.covenant && cha.covenant != 'No covenant') {
			var covenant = DkSUI.contentItem(cha.covenant, 'covenants');
			$('#covenant_icon').append('<img alt="'+covenant.name+'" src="'+covenant.image+'" />');
			$('#covenant_name').text(covenant.name);
		}
		// inventory
		for(i=0; i<2; i++) DkSUI.fillSlot('#right_hand', 'weapons', cha.weapons.rh[i]);
		for(i=0; i<2; i++) DkSUI.fillSlot('#left_hand', 'weapons', cha.weapons.lh[i]);
		for(i=0; i<5; i++) DkSUI.fillSlot('#objects', 'items', cha.items[i]);
		for(i=0; i<2; i++) DkSUI.fillSlot('#arrows', 'quiver', cha.quiver.arrows[i]);
		for(i=0; i<2; i++) DkSUI.fillSlot('#darts', 'quiver', cha.quiver.darts[i]);
		for(k in cha.armour) DkSUI.fillSlot('#armour_'+k, 'armor', cha.armour[k]);
		for(i=0; i<2; i++) DkSUI.fillSlot('#rings', 'rings', cha.rings[i]);
		for(i=0; i<cha.attunement.length; i++) DkSUI.fillSlot('#attunement', 'magic', cha.attunement[i]);
		// stats
		var statk = ['souls', 'points', 'humanity', 'energy', 'atk', 'def', 'resist', 'discovery', 'sorceries'];
		for(i=0; i<statk.length; i++) {
			for(k in cha[statk[i]]) {
				DkSUI.changeStat(statk[i], k, cha[statk[i]][k]);
			}
		}
	},
	changeStat: function(group, stat, value) {
		var id = '#' + group + '_' + stat;
		var label = $(id+'>label')[0];
		if(!label) return;
		var ins;
		if($(label).next().prop('tagName') == 'INS') ins = $(label).next()
		else ins = $('<ins />').insertAfter($(label));
		$(ins).text(value);
	},
	fillSlot: function(element, img, content) {
		var slot = false;
		if($(element).hasClass('slot')) slot = element;
		else {
			slot = $('<div class="slot" />');
			$(element).append($(slot));
		}
		if(!content) return;
		content = DkSUI.contentItem(content, img);
		if(content.isEmpty) return;
		if(content.em) $(slot).append('<em class="'+content.em+'" label="'+DkSUI.upperFirst(content.em)+'" />');
		$(slot).append('<a><img alt="'+content.name+'" src="'+content.image+'" /></a>');
	},
	contentItem: function(content, img) {
		if(typeof content == 'string') content = { name: content };
		content.name = DkSUI.upperFirst(content.name);
		content.name = content.name.replace(/\bRnd\b/gi, 'Round');
		content.name = content.name.replace(/\bdark[^a-z]*moon\b/gi, 'Darkmoon');
		if(!(content.slug)) content.slug = DkSUI.slugify(content);
		img = img.replace(/^\/+|\/+$/g, '');
		if(!img.match(/\.png$/i)) img = 'img/' + img + '/' + content.slug + '.png';
		content.image = img;
		if(content.slug == 'bare-fist') content.isEmpty = true;
		return content;
	},
	upperFirst: function(str, quotes) {
		str = str.replace(/^\s+|\s+$/g, '');
		var arr = str.split(/\s+/g);
		for(var i=0; i<arr.length; i++)
			arr[i] = arr[i][0].toUpperCase() + arr[i].substr(1).toLowerCase();
		str = arr.join(' ');
		if(quotes) str = str.replace(/"/g, "''");
		return str;
	},
	slugify: function(content) {
		if(typeof content == 'string') content = { name: content };
		var slug = content.name.toLowerCase();
		slug = slug.replace(/\s+\+\d+|[^\w\s]+|\s+\(\d+\)$/g, '');
		slug = slug.replace(/\s+|_+/g, '-');
		return slug;
	},
	change_desc: function(title, desc) {
		if(!title) title = '';
		if(!desc) desc = '';
		$('#desc_slot').text(title);
		$('#desc_item').text(desc);
	},
});